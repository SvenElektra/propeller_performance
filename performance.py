#!/usr/bin/python3
import datetime, time, os, os.path, string
import numpy as np
from subprocess import call
import matplotlib
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from operator import add
from functions import RUN_QPROP
from functions import WRITE_QCON
from functions import INTERPOL_DENSITY
from functions import CHECK_ERRORS
import matplotlib as mpl
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D

vertical_speeds=[]

print("############################################");
print("###       Performance Calculation        ###");
print("###            performance.py            ###");
print("############################################");

if (os.path.isfile("input_performance.inp") and os.path.isfile("input_general.inp")  and os.path.isfile("input_calcprop.inp")  and os.path.isfile("motor") and os.path.isfile("propeller") ):
	print("All files existant") 
else:
	print("Inputfiles are not complete. \nYou need: motor, propeller, input_general.inp, input_performance.inp and input_calcprop.inp")
	exit()


line=""
read_input = open("input_performance.inp", 'r')

##### PARSE NAMESTRING #################################
while(line != '#### NAMESTRING ####'):
	line = str.strip(read_input.readline())
namestring = str.strip(read_input.readline())

##### PARSE POLAR DIAGRAM PATH #################################
while(line != '#### POLAR DIAGRAM PATH INCLUDING "/" ####'):
	line = str.strip(read_input.readline())
polardiagram_path = str.strip(read_input.readline())

##### PARSE POLAR DIAGRAM NAME #################################
while(line != '#### POLAR DIAGRAM NAME ####'):
	line = str.strip(read_input.readline())
polardiagram = str.strip(read_input.readline())


##### PARSE MIN DBETA #################################
while(line != '#### MIN. DBETA [Deg.] ####'):
	line = str.strip(read_input.readline())
DBeta_min = float(str.strip(read_input.readline()))


##### PARSE MAX DBETA #################################
while(line != '#### MAX. DBETA[Deg.] ####'):
	line = str.strip(read_input.readline())
DBeta_max = float(str.strip(read_input.readline()))

##### PARSE DELTA DBETA #################################
while(line != '#### DELTA DBETA [Deg.] ####'):
	line = str.strip(read_input.readline())
deltaBeta = float(str.strip(read_input.readline()))

##### PARSE VERTICAL SPEEDS #################################
while(line != '#### VERTICAL SPEEDS [m/s] ####'):
	line = str.strip(read_input.readline())
line = str.strip(read_input.readline())

for i in range (0,len(line.split(","))):
	vertical_speeds.append(float(line.split(",")[i]))

##### PARSE REFERENCE AREA #################################
while(line != '#### REFERENCE AREA [sqm] ####'):
	line = str.strip(read_input.readline())
ref_area = float(str.strip(read_input.readline()))

read_input.close()


readfile_gen = open("input_general.inp", 'r')
##### PARSE SCALEFACTORS TO MODIFY MOTOR AND MC POWER AND CURRENT CONSTANT FROM MEASUREMENT   #################################
while(line != '#### SCALEFACTORS FOR TOTAL-POWER (FIRST) AND CURRENT-CONSTANT (SECOND) ####'):
	line = str.strip(readfile_gen.readline())
line = str.strip(readfile_gen.readline())
scalfac_power = float(line.split(',')[0])
scalfac_curr_const = float(line.split(',')[1])
readfile_gen.close()

namestring = namestring + "_ScalPow_" + str(scalfac_power) + "_ScalCurrConst_" + str(scalfac_curr_const)

#print(namestring,polardiagram,DBeta_min,DBeta_max,deltaBeta,vertical_speeds,ref_area)


#namestring="CD+0.006"
#polardiagram = "12_5km_270kg_26_6m2_CFD-drag-calibrated_CD+0_006.txt"   ### MUST be existent in "/home/sven/PolarData/E2/" #######
#DBeta_min=-13 #degrees
#DBeta_max=10 #degrees
#deltaBeta=0.1 #degrees
#vertical_speeds=[0.0,0.5,1.0,1.5,2.0,2.5,3.0] #m/s
#ref_area=26.6 #square-meters

#####################################################

mass=float(polardiagram[7:10])
altitude=float(polardiagram[0:2])*1000.+float(polardiagram[3:4])*100.
polardiagram=polardiagram_path+polardiagram

print(polardiagram)

f_in = open(polardiagram, 'r')
density=INTERPOL_DENSITY(altitude)
print("Altitude [m]: " + str(altitude)) 
print("Density [kg/cbm] " + str(density)) 
print("Mass [kg] " + str(mass)) 
print("Reference Area [sqm]: " + str(ref_area)) 
print("considered vertical climb speeds [m/s]:" + str(vertical_speeds))

csv_max_tot_eff_out=open("Max_tot_Eff.csv",'w')
csv_max_tot_eff_out.write("altitude[m],velocity[m/s],climbrate[m/s],DBeta[deg],Efficiency_prop[%],Efficiency_EPU[%],Efficiency_TOT[%],RPM,Pprop[W],Pshaft[W],Ptot[W],Thrust[N],Q[Nm],Mot_Current[A],Bat_Current[A],PL_Bat[W],PL_MC[W],PL_PROP[W],PL_MOT_COPPER[W],PL_MOT_BE+FE[W],PL_MOT_TOT[W],PL_PMOT_PMC_MEASURED[W],CURR_CONST_MEASURED[Nm/A]\n")
csv_max_tot_eff_out.close()

rem_dirname='rm -rf Altitude_' + str("%02d" % int(altitude/1000)) + 'km_' + str(int(mass)) + 'kg_' + namestring
mk_dirname='mkdir Altitude_' + str("%02d" % int(altitude/1000)) + 'km_' + str(int(mass)) + 'kg_' + namestring
move_to_dir='mv vel_* All_Velocities Reduced_Sink.csv Max_tot_Eff.csv Altitude_' + str("%02d" % int(altitude/1000)) + 'km_' + str(int(mass)) + 'kg_' + namestring
copy_polardiagram='cp ' + str(polardiagram) + ' ' + 'Altitude_' + str("%02d" % int(altitude/1000)) + 'km_' + str(int(mass)) + 'kg_' + namestring

os.system(rem_dirname)
os.system(mk_dirname)
os.system('rm -rf vel_*')
os.system('rm -rf All_Velocities')
os.system(copy_polardiagram)

line="dummy"
velocity=[]
CD=[]
CL=[]
DRAG=[]
LIFT=[]


#### find the first data-line #################################
while(line != ''):
	line = f_in.readline()
	if line.strip():
		if (len(str.split(line.strip())) > 3):
			if (str.split(line.strip())[0] == "alpha"):				
				line=''

#### read out data #################################				
line="dummy"
while(line != ''):
	line = str.strip(f_in.readline())
	if len(line)>0:
		#print str.split(line)[11]	
		CL.append(float(str.split(line)[2]))
		CD.append(float(str.split(line)[5]))
		velocity.append(float(str.split(line)[11]))		
	
f_in.close()

for i in range(0,len(CD)):
	LIFT.append(0.5*float(density)*velocity[i]*velocity[i]*ref_area*CL[i])
	DRAG.append(0.5*float(density)*velocity[i]*velocity[i]*ref_area*CD[i])



######### Calculate Reduced Sink with low Power ##########################################

csv_redsink_out=open("Reduced_Sink.csv",'w')
csv_redsink_out.write("altitude[m],velocity[m/s],climbrate[m/s],DBeta[deg],Efficiency_prop[%],Efficiency_EPU[%],Efficiency_TOT[%],RPM,Pprop[W],Pshaft[W],Ptot[W],Thrust[N],Q[Nm],Mot_Current[A],Bat_Current[A],PL_Bat[W],PL_MC[W],PL_PROP[W],PL_MOT_COPPER[W],PL_MOT_BE+FE[W],PL_MOT_TOT[W],PL_PMOT_PMC_MEASURED[W],CURR_CONST_MEASURED[Nm/A]\n")
csv_redsink_out.close()

### Determine the velocity with lowest Sinkrate #####
v_sink=[]
for i in range(0,len(LIFT)):
	v_sink.append(velocity[i]*DRAG[i]/LIFT[i])

v_sink_min=min(v_sink)
index=np.argmin(v_sink)

f_out = open("input_calcprop.inp", 'w')
f_out.write("#### ALTITUDE [m] ####\n")
f_out.write(str(altitude) + "\n")
f_out.write("#### VELOCITY [m/s] ####\n")    
f_out.write(str(velocity[index]) + "\n")
f_out.write("#### PROPELLER POWER [W] ####\n")

PPROPSINK=[]
PPROPSINK.append(int(velocity[index]*DRAG[index]*0.10)) #####  10% of Hover Power #######
PPROPSINK.append(int(velocity[index]*DRAG[index]*0.25))	#####  25% of Hover Power #######
PPROPSINK.append(int(velocity[index]*DRAG[index]*0.50))	#####  50% of Hover Power #######
PPROPSINK.append(int(velocity[index]*DRAG[index]*0.75))	#####  75% of Hover Power #######
PPROPSINK.append(int(velocity[index]*DRAG[index]*1.00))	##### 100% of Hover Power #######

f_out.write(str(PPROPSINK[0]))	
f_out.write(str(","))
f_out.write(str(PPROPSINK[1]))
f_out.write(str(","))
f_out.write(str(PPROPSINK[2]))
f_out.write(str(","))
f_out.write(str(PPROPSINK[3]))
f_out.write(str(","))
f_out.write(str(PPROPSINK[4]))
f_out.write(str("\n"))

f_out.write("#### MIN. DBETA [Deg.] ####\n")
f_out.write(str(DBeta_min) + "\n")
f_out.write("#### MAX. DBETA[Deg.] ####\n")
f_out.write(str(DBeta_max) + "\n")
f_out.write("#### DELTA DBETA [Deg.] ####\n")        
f_out.write(str(deltaBeta) + "\n")
f_out.write("#### CLIMB + HOVER (1) or SINK + HOVER(0) ####\n")        
f_out.write(str(0) + "\n")
f_out.write("#### DRAG [N] - ######## OPTIONAL ########\n")        
f_out.write(str(DRAG[index]) + "\n")
f_out.write("#### MASS [kg] - ######## OPTIONAL ########\n")        
f_out.write(str(mass) + "\n")

f_out.close()

REDSINK=np.zeros(5)
REDSINK_STR=np.zeros(5)

for i in range (0,5):
	REDSINK[i]=PPROPSINK[i]/LIFT[index]-velocity[index]*DRAG[index]/LIFT[index]

for i in range(0,len(REDSINK)):
	REDSINK_STR[i]=str(round(REDSINK[i],2))

print("\n############################################################################")
print("#################   SUBSECTION REDUCED SINK AND HOVER ######################")
print("############################################################################")

print("\n#### Starting Propeller-Simulation for Reduced Sink and Hover with following Parameters: ################\n\nAltitude [m]: " + str(altitude) + "\nVelocity [m/s]: " + str(velocity[index]) + "\nPropeller-Power [W]: " + str(PPROPSINK)+ \
"\nClimbrates [m/s]: " + str(REDSINK_STR) +"\nDBeta min. [deg]: " + str(DBeta_min) + "\nDBeta max.[deg]: " + str(DBeta_max) + "\ndelta beta [deg]: "+ str(deltaBeta) + "\nMass[kg]: " + str(mass) + "\nDrag[N]: " + str(DRAG[i]) + "\n")



os.system("/home/sven/Python/Propeller_Performance/calcprop.py ")

abort = CHECK_ERRORS()
if (abort == 1):
	exit()

del(PPROPSINK)


######### Calculate Hover and  Climb ######################################################

for i in range(0,len(velocity)):
	f_out = open("input_calcprop.inp", 'w')
	f_out.write("#### ALTITUDE [m] ####\n")
	f_out.write(str(altitude) + "\n")
	f_out.write("#### VELOCITY [m/s] ####\n")    
	f_out.write(str(velocity[i]) + "\n")
	f_out.write("#### PROPELLER POWER [W] ####\n")
	
	PPROP=[]
	for j in range(len(vertical_speeds)-1):
		PPROP.append(str(int(DRAG[i]*velocity[i] + vertical_speeds[j]*mass*9.81))+",")
		f_out.write(PPROP[j])
	PPROP.append(str(int(DRAG[i]*velocity[i] + vertical_speeds[j+1]*mass*9.81)) + "\n")
	f_out.write(PPROP[j+1])	
	
	f_out.write("#### MIN. DBETA [Deg.] ####\n")
	f_out.write(str(DBeta_min) + "\n")
	f_out.write("#### MAX. DBETA[Deg.] ####\n")
	f_out.write(str(DBeta_max) + "\n")
	f_out.write("#### DELTA DBETA [Deg.] ####\n")        
	f_out.write(str(deltaBeta) + "\n")
	f_out.write("#### CLIMB + HOVER (1) or SINK + HOVER(0) ####\n")        
	f_out.write(str(1) + "\n")
	f_out.write("#### DRAG [N] - ######## OPTIONAL ########\n")        
	f_out.write(str(DRAG[i]) + "\n")
	f_out.write("#### MASS [kg] - ######## OPTIONAL ########\n")        
	f_out.write(str(mass) + "\n")
	
	f_out.close()
	print("\n#### Starting Propeller-Simulation for Climb and Hover with following Parameters: ################\n\nAltitude [m]: " + str(altitude) + "\nVelocity [m/s]: " + str(velocity[i]) + "\nPropeller-Power [W]: " + str(PPROP)+ \
	"\nClimbrates [m/s]: " + str(vertical_speeds) +"\nDBeta min. [deg]: " + str(DBeta_min) + "\nDBeta max.[deg]: " + str(DBeta_max) + "\ndelta beta [deg]: "+ str(deltaBeta) + "\nMass[kg]: " + str(mass) + "\nDrag[N]: " + str(DRAG[i]) + "\n")
	
	print("\n############################################################################")
	print("#################          SUBSECTION CLIMB            #####################")
	print("############################################################################")
	
	print("\n\n#### Velocities to be analyzed (m/s)",velocity)
	os.system("/home/sven/Python/Propeller_Performance/calcprop.py")
	abort = CHECK_ERRORS()
	if (abort == 1):
		exit()
	
	del PPROP [:]

os.system(move_to_dir)
