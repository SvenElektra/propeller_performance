#!/usr/bin/python3
import datetime, time, os, os.path, string
import numpy as np
from subprocess import call
import matplotlib
matplotlib.use('QT5Agg')
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from operator import add
from functions import RUN_QPROP
from functions import WRITE_QCON
import matplotlib as mpl
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import RegularGridInterpolator
import csv

filename=[]
num_cols=[]
row_count=[]

filename.append("/home/sven/Propeller/Prop_Performance/E2_H40A_2.2m_R-EA-2-BLADE/Altitude_00km_385kg_E2-PLUS_CD+0.0055_Prop-0km_8Batt_HPD32D_MEASURED/Max_tot_Eff.csv")
filename.append("/home/sven/Propeller/Prop_Performance/E2_H40A_2.2m_R-EA-2-BLADE/Altitude_02km_385kg_E2-PLUS_CD+0.0055_Prop-0km_8Batt_HPD32D_MEASURED/Max_tot_Eff.csv")
filename.append("/home/sven/Propeller/Prop_Performance/E2_H40A_2.2m_R-EA-2-BLADE/Altitude_05km_385kg_E2-PLUS_CD+0.0055_Prop-0km_8Batt_HPD32D_MEASURED/Max_tot_Eff.csv")

filename.append("/home/sven/Propeller/Prop_Performance/E2_H40A_2.2m_R-EA-2-BLADE/Altitude_07km_385kg_E2-PLUS_CD+0.0055_Prop-10km_8Batt_HPD32D_MEASURED/Max_tot_Eff.csv")
filename.append("/home/sven/Propeller/Prop_Performance/E2_H40A_2.2m_R-EA-2-BLADE/Altitude_10km_385kg_E2-PLUS_CD+0.0055_Prop-10km_8Batt_HPD32D_MEASURED/Max_tot_Eff.csv")
filename.append("/home/sven/Propeller/Prop_Performance/E2_H40A_2.2m_R-EA-2-BLADE/Altitude_12km_385kg_E2-PLUS_CD+0.0055_Prop-10km_8Batt_HPD32D_MEASURED/Max_tot_Eff.csv")

filename.append("/home/sven/Propeller/Prop_Performance/E2_H40A_2.2m_R-EA-2-BLADE/Altitude_15km_385kg_E2-PLUS_CD+0.0055_Prop-20km_8Batt_HPD32D_MEASURED/Max_tot_Eff.csv")
filename.append("/home/sven/Propeller/Prop_Performance/E2_H40A_2.2m_R-EA-2-BLADE/Altitude_17km_385kg_E2-PLUS_CD+0.0055_Prop-20km_8Batt_HPD32D_MEASURED/Max_tot_Eff.csv")
filename.append("/home/sven/Propeller/Prop_Performance/E2_H40A_2.2m_R-EA-2-BLADE/Altitude_20km_385kg_E2-PLUS_CD+0.0055_Prop-20km_8Batt_HPD32D_MEASURED/Max_tot_Eff.csv")

for i in range(0,len(filename)):
	f = open(filename[i],'r')
	reader = csv.reader(f,delimiter=',')
	num_cols.append(len(next(reader)))
	row_count.append(sum(1 for row in reader))
	f.close()

for i in range(0,len(filename)):
	print("Number of Lines: ",row_count[i]," in ",filename[i])

	
if(min(row_count) != max(row_count)):
	print("\n############# WARNING: SOME LOAD CASES HAVE NOT BEEN CALCULATED, CHECK DIFFERENT LINE DIMENSIONS OF FILES ########################\n")

