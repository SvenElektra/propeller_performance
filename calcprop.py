#!/usr/bin/python3
import datetime, time, os, os.path, string, sys
import numpy as np
from subprocess import call
import matplotlib
matplotlib.use('QT5Agg')
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from operator import add
from functions import RUN_QPROP
from functions import WRITE_QCON
import matplotlib as mpl
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import RegularGridInterpolator

print("\n\n\n############################################");
print("###         Propeller Calculation        ###");
print("###               Calcprop.py            ###");
print("############################################");
line=""

if (os.path.isfile("input_calcprop.inp") and os.path.isfile("input_general.inp")  and os.path.isfile("motor") and os.path.isfile("propeller") ):
	print("\n#### Remark: All Inputfiles existant")
else:
	print("Inputfiles are not complete. \nYou need: motor, propeller, input_general.inp and input_calcprop.inp")
	exit()

#### READ IN GENERAL DATA ####

readfile_gen = open("input_general.inp", 'r')

##### PARSE PLOTFLAG #################################
while(line != '#### PLOTFLAG 1-YES, 0-NO ####'):
	line = str.strip(readfile_gen.readline())
plotflag = float(str.strip(readfile_gen.readline()))

##### PARSE NUMBER OF MOTORS #################################
while(line != '#### NUMBER OF MOTORS ####'):
	line = str.strip(readfile_gen.readline())
mot_count = int(str.strip(readfile_gen.readline()))

##### PARSE NUMBER OF BATTERY PACKS PER SET #################################
while(line != '#### NUMBER OF BATTERY PACKS PER SET ####'):
	line = str.strip(readfile_gen.readline())
bat_packs_per_set = int(str.strip(readfile_gen.readline()))

##### PARSE INTERNAL BATTERY RESISTANCE PER PACK  #################################
while(line != '#### INTERNAL BATTERY RESISTANCE PER PACK [OHM] ####'):
	line = str.strip(readfile_gen.readline())
bat_res = float(str.strip(readfile_gen.readline()))

##### PARSE AVERAGE BATTERY VOLTAGE  #################################
while(line != '#### AVERAGE BATTERY VOLTAGE [V] ####'):
	line = str.strip(readfile_gen.readline())
bat_volt_av = float(str.strip(readfile_gen.readline()))

##### PARSE PARAMETER FOR MOTOR FE- AND BEARING LOSS  #################################
while(line != '#### PARAMETER FOR MOTOR FE- AND BEARING LOSS ####'):
	line = str.strip(readfile_gen.readline())
mot_fe_brg_loss = float(str.strip(readfile_gen.readline()))

##### PARSE INTERNAL MOTOR RESISTANCE  #################################
while(line != '#### INTERNAL MOTOR RESISTANCE [OHM] ####'):
	line = str.strip(readfile_gen.readline())
mot_cu_res = float(str.strip(readfile_gen.readline()))

##### PARSE INTERNAL MOTOR CONTROLLER RESISTANCE  #################################
while(line != '#### INTERNAL MOTOR CONTROLLER RESISTANCE [OHM] ####'):
	line = str.strip(readfile_gen.readline())
mc_resistance = float(str.strip(readfile_gen.readline()))

##### PARSE MOTOR CONTROLLER LOSS PARAMETER  #################################
while(line != '#### MOTOR CONTROLLER LOSS PARAMETER ####'):
	line = str.strip(readfile_gen.readline())
mc_loss_par = float(str.strip(readfile_gen.readline()))

##### PARSE ADDITIONAL MOTOR CONTROLLER LOSS DUE TO NON-EXISTANT ACTIVE FREEWHEELING  #################################
while(line != '#### ADDITIONAL MOTOR CONTROLLER LOSS DUE TO NON-EXISTANT ACTIVE FREEWHEELING####'):
	line = str.strip(readfile_gen.readline())
mc_loss_no_freewheel = float(str.strip(readfile_gen.readline()))

##### PARSE IF MEASURED EPU EFFICIENCY DATA IS AVAILABLE  #################################
while(line != '#### MEASURED EPU EFFICIENCY DATA AVAILABLE? 0 OR 1 ####'):
	line = str.strip(readfile_gen.readline())
epu_measurement_data = float(str.strip(readfile_gen.readline()))

##### PARSE SCALEFACTORS TO MODIFY MOTOR AND MC POWER AND CURRENT CONSTANT FROM MEASUREMENT   #################################
while(line != '#### SCALEFACTORS FOR TOTAL-POWER (FIRST) AND CURRENT-CONSTANT (SECOND) ####'):
	line = str.strip(readfile_gen.readline())
line = str.strip(readfile_gen.readline())
scalfac_power = float(line.split(',')[0])
scalfac_curr_const = float(line.split(',')[1])


if epu_measurement_data == 1.0:
	
	################################################## POWER / EFFICIENCY DATA ##############################################################################################
	
	##### PARSE MEASURED EPU EFFICIENCY DATA PATH  #################################
	while(line != '#### MEASURED EPU EFFICIENCY DATA FILE ####'):
		line = str.strip(readfile_gen.readline())
	filename_measured_mot_data = str.strip(readfile_gen.readline())
	fn_power=filename_measured_mot_data

	##### PARSE MEASURED EPU EFFICIENCY DATA #################################
	measured_epu_data=[]
	measured_speeds=[]
	
	#print("\n\n########### ATTENTION: CSV-FILE MUST NOT CONTAIN EMPTY LINES! MAKE SURE THAT ONLY DATA LINES ARE CONTAINED! ########")
	
	#### READ THE CSV-FILE WITH THE DATA, DETERMINE DIMENSIONS ##############
	readfile_measured_mot_data = open(filename_measured_mot_data, 'r')
	linecount=1
	torque_counter=[]
	while True :
		line = readfile_measured_mot_data.readline()
		if ("" == line):
			break
		else:
			if linecount == 1:
				measured_speeds_tmp=np.delete(str.split(str.strip(line),","),0)
			if linecount != 1:
				torque_counter.append(float(str.split(str.strip(line),",")[0]))
				
		linecount=linecount+1
	torques=torque_counter
	del torque_counter
	readfile_measured_mot_data.close()
	
	for j in range(0,len(measured_speeds_tmp)):
			measured_speeds.append(float(measured_speeds_tmp[j]))
		
	measured_EPU_input_power = np.zeros((len(torques),len(measured_speeds)))

	readfile_measured_mot_data = open(filename_measured_mot_data, 'r')
	linecount=1
	for j in range(0,len(torques)+1):
		line=readfile_measured_mot_data.readline()
		if linecount!=1:
			for k in range(0,len(measured_speeds)):
				measured_EPU_input_power[j-1][k] = float(str.split(str.strip(line),",")[k+1]) * scalfac_power
		linecount=linecount+1
	readfile_measured_mot_data.close()
	INTERPOLATE_INPUT_POWER = RegularGridInterpolator((torques, measured_speeds), measured_EPU_input_power)
	

	RPM_min_meas=np.min(measured_speeds)
	RPM_max_meas=np.max(measured_speeds)
	torque_min_meas=np.min(torques)
	torque_max_meas=np.max(torques)
	
	#print("Min. Speed: ", RPM_min_meas)
	#print("Min. Torque: ",torque_min_meas)
	
	#print("Max. Speed: ", RPM_max_meas)
	#print("Max. Torque: ",torque_max_meas)
	#print(measured_EPU_input_power)

	################################################## CURRENT CONSTANT DATA ##############################################################################################

	##### PARSE MEASURED CURRENT CONSTANT DATA PATH  #################################
	while(line != '#### MEASURED EPU CURRENT CONSTANT DATA FILE ####'):
		line = str.strip(readfile_gen.readline())
	filename_measured_mot_data = str.strip(readfile_gen.readline())
	fn_const=filename_measured_mot_data
	
	measured_EPU_current_constants = np.zeros((len(torques),len(measured_speeds)))

	readfile_measured_mot_data = open(filename_measured_mot_data, 'r')
	linecount=1
	for j in range(0,len(torques)+1):
		line=readfile_measured_mot_data.readline()
		if linecount!=1:
			for k in range(0,len(measured_speeds)):
				measured_EPU_current_constants[j-1][k] = float(str.split(str.strip(line),",")[k+1]) * scalfac_curr_const
		linecount=linecount+1
	readfile_measured_mot_data.close()
	INTERPOLATE_CURRENT_CONSTANT = RegularGridInterpolator((torques, measured_speeds), measured_EPU_current_constants)



##### PARSE PENALTY FACTOR TO ACCOUNT FOR BET UNDERPREDICTION OF PROPELLER TORQUE   #################################
while(line != '#### PENALTY FACTOR TO ACCOUNT FOR BET UNDERPREDICTION OF PROPELLER TORQUE (USUALLY 5% - 10%) ####'):
	line = str.strip(readfile_gen.readline())
eff_penalty = float(str.strip(readfile_gen.readline()))

##### PARSE MOTOR CURRENT CONSTANT  #################################
while(line != '#### MOTOR CURRENT CONSTANT (PER MOTOR) ####'):
	line = str.strip(readfile_gen.readline())
motor_current_constant = float(str.strip(readfile_gen.readline()))

##### PARSE MAXIMUM MOTOR CURRENT PER MOTOR  #################################
while(line != '#### MAXIMUM MOTOR CURRENT PER MOTOR [A] ####'):
	line = str.strip(readfile_gen.readline())
maxcurrent_per_motor = float(str.strip(readfile_gen.readline()))

##### PARSE PEAK MOTOR CURRENT PER MOTOR  #################################
while(line != '#### PEAK MOTOR CURRENT PER MOTOR [A] ####'):
	line = str.strip(readfile_gen.readline())
peakcurrent_per_motor = float(str.strip(readfile_gen.readline()))
readfile_gen.close()


##### READ IN RUN-DATA ############################

f_in = open("input_calcprop.inp", 'r')
f_in.readline()
act_alt=float(str.strip(f_in.readline()))
f_in.readline()
velocity=float(str.strip(f_in.readline()))
print("\n#### Velocity = %2.1f m/s" %(velocity))
f_in.readline()
Pprop_str=str.strip(f_in.readline())
f_in.readline()
min_delta_beta=float(str.strip(f_in.readline()))
f_in.readline()
max_delta_beta=float(str.strip(f_in.readline()))
f_in.readline()
deltabeta=float(str.strip(f_in.readline()))
f_in.readline()
climb_sink_flag=int(str.strip(f_in.readline()))
f_in.readline()
drag=float(str.strip(f_in.readline()))
f_in.readline()
mass=float(str.strip(f_in.readline()))
f_in.close()

Pprop=[]
for count in range(0,len(str.split(Pprop_str,','))):
	Pprop.append(int(str.split(Pprop_str,',')[count]))

Climbrate=[]
print("\n#### Considered Propeller Power Settings and corresponding Climbrates")
for count in range(0,len(str.split(Pprop_str,','))):
	Climbrate.append((Pprop[count]-velocity*drag)/mass/9.81)
	print("Prop-Power= " + str(Pprop[count]) + " W --> Climbrate= " + "%.2f" % float((Pprop[count]-velocity*drag)/mass/9.81)+" m/s")

maxmoment = maxcurrent_per_motor*mot_count*motor_current_constant # maximum motor moment of all motors together [Nm]
peakmoment = peakcurrent_per_motor*mot_count*motor_current_constant # peak motor moment of all motors together [Nm]



RPM=[]
Dbeta=[]
Q=[]
Thrust=[]
Current=[]
Current_Battery=[]
Pshaft =[]
ARR_Pprop =[]
Ptot =[]

PL_Prop =[]
PL_Pbat =[]
PL_PMC =[]
PL_PMOT =[]
PL_PMOT_CU =[]
PL_PMOT_FE_BEA =[]
PL_PMOT_PMC_MEASURED=[]
CURR_CONST_MEASURED=[]



# Maxeff of propeller only
Efficiency_prop=[]
Maxeff_prop=[]
Maxeff_prop_Dbeta=[]
Maxeff_prop_Q=[]
Maxeff_prop_RPM=[]
Maxeff_prop_Thrust=[]
Maxeff_prop_Current=[]
Maxeff_prop_Current_Battery=[]
Maxeff_prop_Pshaft=[]
Maxeff_prop_PLbat=[]
Maxeff_prop_PLPMC=[]
Maxeff_prop_PLPMOT=[]
Maxeff_prop_PL_Prop=[]
Maxeff_prop_PLPMOT_PMC_MEASURED=[]

# Maxeff EPU total
Efficiency_EPU=[]
Maxeff_EPU=[]
Maxeff_EPU_Dbeta=[]
Maxeff_EPU_RPM=[]
Maxeff_EPU_Q=[]
Maxeff_EPU_Current=[]
Maxeff_EPU_Current_Battery=[]
Maxeff_EPU_Pshaft=[]
Maxeff_EPU_PLbat=[]
Maxeff_EPU_PLPMC=[]
Maxeff_EPU_PLPMOT=[]
Maxeff_EPU_PL_Prop=[]
Maxeff_EPU_Ptot=[]
Maxeff_EPU_MOT_MC_MEASURED=[]

# Maxeff System total, including EPU and Aircraft
Efficiency_TOT=[]
Maxeff_TOT=[]
Maxeff_TOT_Dbeta=[]
Maxeff_TOT_RPM=[]
Maxeff_TOT_Q=[]
Maxeff_TOT_Current=[]
Maxeff_TOT_Current_Battery=[]
Maxeff_TOT_PLbat=[]
Maxeff_TOT_PLPMC=[]
Maxeff_TOT_PLPMOT=[]
Maxeff_TOT_PL_Prop=[]
Maxeff_TOT_Ptot=[]
Maxeff_TOT_MOT_MC_MEASURED=[]

number_beta=int((max_delta_beta-min_delta_beta)/deltabeta)+1

### Interpolates atmospheric data for the requested altitude and creates the file "qcon.def" for QPROP
atmo=WRITE_QCON(act_alt) 

#### Execute QPROP and create outputfiles with results ###
for i in range(0,len(Pprop)):
	RUN_QPROP(velocity,Pprop[i],number_beta, min_delta_beta, max_delta_beta, deltabeta)
	errorcheck = open('QPROP.error', 'r')
	if "IEEE_INVALID_FLAG" in errorcheck.readline():
		print("\n### ERROR, CALCULATION ABORTED. DBETA IS PROBABLY BEYOND ACCEPTABLE RANGE! ###\n")
		os.system('rm -rf vel* All* out*W')
		exit()
	errorcheck.close()


csv_filename="csv_v"+str(velocity)+"ms-h"+str(act_alt)+"m.csv"
csv_out = open(csv_filename, 'w')
csv_out.write("altitude[m],velocity[m/s],climbrate[m/s],DBeta[deg],Efficiency_prop[%],Efficiency_EPU[%],Efficiency_TOT[%],RPM,Pprop[W],Pshaft[W],Ptot[W],Thrust[N],Q[Nm],Mot_Current[A],Bat_Current[A],PL_Bat[W],PL_MC[W],PL_PROP[W],PL_MOT_COPPER[W],PL_MOT_BE+FE[W],PL_MOT_TOT[W],PL_MOT_MC_MEASURED[W],CURR_CONST_MEASURED[Nm/A]\n")




if climb_sink_flag == 1:
	csv_max_tot_eff_out=open("Max_tot_Eff.csv",'a')

if climb_sink_flag == 0:
	csv_max_tot_eff_out=open("Reduced_Sink.csv",'a')

if(os.path.isfile('EPU_DATA.error')):
	os.system('rm EPU_DATA.error')


#### Plotting Limits for Diagrams ####
Pmax=0

for i in range(0,len(Pprop)):
	cmap = plt.get_cmap('jet_r')
	
	if len(Pprop)==1:
		lcolor = cmap(1)
	if len(Pprop)>1:
		lcolor = cmap(float(1./(len(Pprop)-1)*i))

	filename='out_' + str(Pprop[i]) + 'W'
	f_in = open(filename, 'r')
	line = "dummy"
	while(line != ''):
		line = f_in.readline()
		if line.strip():
			if (len(str.split(line.strip())) > 15):
				#print str.split(line.strip())[1]
				if (str.split(line.strip())[1] == "V(m/s)"):
					line = f_in.readline()
					while line[1:3] =="GV":
						line = f_in.readline()
						##print("Convergence issue detected in " + filename)
					RPM.append(float(str.split(line.strip())[2]))
					Dbeta.append(float(str.split(line.strip())[3]))
					Thrust.append(float(str.split(line.strip())[4]))
					Q.append(float(str.split(line.strip())[5]))
					Efficiency_prop.append(float(line[94:100]) / eff_penalty)

	f_in.close()
	
	# Account for theory overpredicting propeller efficiency ###################################
	for j in range(0,number_beta):
		Q[j] = float(Q[j]) * eff_penalty
	
		
	# Compute total Current ###################################
	for j in range(0,number_beta):
		if epu_measurement_data == 1:
			if( Q[j] < torque_min_meas  or Q[j] > torque_max_meas or RPM[j] < RPM_min_meas or RPM[j] > RPM_max_meas):
				print("\n\n#### MOTOR EFFICIENCY MEASUREMENT DATA RANGE VIOLATED ####")
				print("\nVelocity= %3.1fm/s\nCalculated Torque=%3.1fNm, Calculated RPM=%3.0f" % (velocity,Q[j],RPM[j]))
				print("Torque measured min.= %3.1fNm \nTorque measured max.= %3.1fNm \nRPM measured min.=%3.0f \nRPM measured max.=%3.0f" % (torque_min_meas,torque_max_meas,RPM_min_meas,RPM_max_meas))
				print("\nMeasurement Data is only available for the following Operating Points:")
				print("\nMotor Efficiency Measuremenet Data - RPM Values: ", measured_speeds)
				print("Motor Efficiency Measuremenet Data - Torque Values: ",torques)
				cmd0="rm csv*csv Red*csv out*"
				os.system(cmd0)
				epu_errorfile=open('EPU_DATA.error','w')
				epu_errorfile.write('#### MOTOR EFFICIENCY MEASUREMENT DATA RANGE VIOLATED ####')
				epu_errorfile.close()
				exit()
			else:				
				motor_current_constant=INTERPOLATE_CURRENT_CONSTANT(np.array([Q[j], RPM[j]]))[0]
				CURR_CONST_MEASURED.append(motor_current_constant)
		if epu_measurement_data == 0:
			CURR_CONST_MEASURED.append(0.0)
		
		Current.append(float(Q[j])/float(motor_current_constant))

					
	# Determine Values with best propeller efficiency
	Maxeff_prop.append(max(Efficiency_prop[:]))
	Maxeff_prop_Dbeta.append(Dbeta[Efficiency_prop.index(max(Efficiency_prop[:]))])
	Maxeff_prop_Thrust.append(Thrust[Efficiency_prop.index(max(Efficiency_prop[:]))])
	Maxeff_prop_Q.append(Q[Efficiency_prop.index(max(Efficiency_prop[:]))])
	Maxeff_prop_RPM.append(RPM[Efficiency_prop.index(max(Efficiency_prop[:]))])
	Maxeff_prop_Current.append(Current[Efficiency_prop.index(max(Efficiency_prop[:]))])	

	################################################################################################################
	################################################################################################################
	################                                                                                ################
	################            PLOTTING SECTION                                                    ################
	################                                                                                ################
	################################################################################################################
	################################################################################################################

	
	##### ZEROTH FIGURE (TEXT OUTPUT) ##########################################
	plot_0 = plt.figure(0, figsize=(25,8))
	
	try:
		dummy=plot_0_subplot0	
	except:
		plot_0_subplot0=plot_0.add_subplot(1,2,1)
		
	plot_0_subplot0.axes.get_xaxis().set_visible(False)
	plot_0_subplot0.axes.get_yaxis().set_visible(False)
		
	if epu_measurement_data == 1:
		motor_current_constant="N/A"
		plot_0_subplot0.text(0.1,0.02," #### EPU EFFICIENCY DATA BASED ON MEASUREMENT ####\nDatafiles:"+str(fn_power)+" and \n" +str(fn_const))
		
	plot_0_subplot0.text(0.1,0.1,\
	"######## ELEKTRA II #########\n\n"+\
	"Flight Altitude= "+str(act_alt)+" [m] MSL \n"+\
	"Velocity= "+ str(velocity) + " [m/s] \n"+\
	"Density= " + str(atmo[0]) + " [kg/m^3]\n"+\
	"mue=" + str(atmo[1]) + "[kg/m-s]\n"+\
	"c=" + str(atmo[2]) + "[m/s]\n"+\
	"Propeller Power Values prescribed: " + str(Pprop) +"[W] \n\n" +\
	\
	"Number of Battery Packs per Motor: " + str(bat_packs_per_set) + "\n"+\
	"Total Number of Motors: " + str(mot_count) +"\n" +\
	"Total Number of Battery Packs: " + str(bat_packs_per_set*mot_count) +"\n" +\
	"Max. Motor Shaft Moment (total)=" + str(maxmoment) + " [Nm]\n"+\
	"Peak Motor Shaft Moment (total)=" + str(peakmoment) + " [Nm]\n"+\
	
	"Motor Current Constant (per Motor)=" + str(motor_current_constant) + " [Nm/A]\n"+\
	"Max. Motor Current (per Motor)=" + str(maxcurrent_per_motor) + " [A]\n"+\
	"Peak Motor Current (per Motor)=" + str(peakcurrent_per_motor) + " [A]\n\n"+\
	\
	"Battery Resistance per Pack: " + str(bat_res) + " [Ohm]\n"+\
	"Battery Average Voltage: " + str(bat_volt_av) + " [V]\n\n"+\
	\
	"Motor Controller Resistance (per Controller unit): " + str(mc_resistance) + " [Ohm]\n"+\
	"Motor Controller Loss Parameter (per Controller unit): " + str(mc_loss_par) + " [-]\n"+\
	"Motor Controller Loss Factor due to non-existant freewheeling: " + str(mc_loss_no_freewheel) + " [-]\n\n"+\
	\
	"Motor Resistance (per Motor, Copper Windings): " +  str(mot_cu_res) + " [Ohm]\n"+\
	"Motor Bearing and FE-Loss Parameter (per Motor): " +  str(mot_fe_brg_loss) + " [-]\n\n"
	\
	"Factor to account for BET underprediction of torque (scale torque manually): " +  str(eff_penalty) + " [-]\n"+\
	"Additional controller loss factor to account for non-existant freewheeling: " +  str(mc_loss_no_freewheel) + " [-]\n")
	
	try:
		dummy=plot_0_subplot1
	except:
		plot_0_subplot1=plot_0.add_subplot(1,2,2)
		
	plot_0_subplot1.axes.get_xaxis().set_visible(False)
	plot_0_subplot1.axes.get_yaxis().set_visible(False)
	img=mpimg.imread('/home/sven/Python/Propeller_Performance/formulas.png')
	imgplot = plt.imshow(img)
	plt.savefig('plot0.png')
		
	##### FIRST FIGURE ##########################################
	plot_1 = plt.figure(1, figsize=(15,12))
	ctitle = "Altitude above MSL="+str(act_alt)+" [m], Velocity= "+ str(velocity) + "[m/s], density= " + atmo[0] + " [kg/m^3], mue=" + atmo[1] + "[kg/m-s], c=" + atmo[2] + "[m/s]"
	diaglabel = "P_prop: " + str(Pprop[i]) + 'W'
	plot_1.suptitle(ctitle, fontsize=12)

	##### Efficiency #########################################
	try:
		dummy=plot_1_subplot1	
	except:
		plot_1_subplot1=plot_1.add_subplot(2, 3, 1)
	plot_1_subplot1.plot(Dbeta[:], Efficiency_prop[:],'-',label=diaglabel,color=lcolor)
	plt.ylim([0,1])
	plt.xlabel('Dbeta [deg]',fontsize=10)
	plt.ylabel('Efficiency [-]',fontsize=10)
	plt.grid(b=True,which='major',color='b',linestyle='--')
	plt.title('Efficiency')
	
	##### RPM #########################################
	try:
		dummy=plot_1_subplot2
	except:
		plot_1_subplot2=plot_1.add_subplot(2, 3, 2,sharex=plot_1_subplot1)
	plot_1_subplot2.plot(Dbeta[:], RPM[:],'-',label=diaglabel,color=lcolor)
	plt.legend(loc='upper right',fontsize=12)
	plt.xlabel('Dbeta [deg]',fontsize=10)
	plt.ylabel('RPM [1/min]',fontsize=10)
	plt.grid(b=True,which='major',color='b',linestyle='--')
	plt.title('RPM')
	
	##### Moment #########################################
	try:
		dummy=plot_1_subplot3
	except:
		plot_1_subplot3=plot_1.add_subplot(2, 3, 3,sharex=plot_1_subplot1)
		
	plot_1_subplot3.plot(Dbeta[:], Q[:],'-',label=diaglabel,color=lcolor)
	
	plt.xlabel('Dbeta [deg]',fontsize=10)
	plt.ylabel('Moment [Nm]',fontsize=10)
	plt.grid(b=True,which='major',color='b',linestyle='--')
	plt.title('Moment')
	sym_Qnom = matplotlib.lines.Line2D([], [], color='red',marker='',markersize=0,linestyle='-.',linewidth=2.0,label='Q_Max')
	sym_Qpeak = matplotlib.lines.Line2D([], [], color='red',marker='',markersize=0,linestyle='-',linewidth=2.0,label='Q_Peak')	

	handles = [sym_Qpeak, sym_Qnom]
	labels = [h.get_label() for h in handles]
	plot_1_subplot3.legend(handles=handles, labels=labels,fontsize=12) 
	
	##### Thrust #########################################
	try:
		dummy=plot_1_subplot4	
	except:
		plot_1_subplot4=plot_1.add_subplot(2, 3, 4,sharex=plot_1_subplot1)
		
	plot_1_subplot4.plot(Dbeta[:], Thrust[:],'-',label=diaglabel,color=lcolor)
	plt.xlabel('Dbeta [deg]',fontsize=10)
	plt.ylabel('Thrust [N]',fontsize=10)
	plt.grid(b=True,which='major',color='b',linestyle='--')
	plt.title('Thrust')
	##### Power Losses #########################################
	try:
		dummy=plot_1_subplot6	
	except:
		plot_1_subplot6=plot_1.add_subplot(2, 3, 6,sharex=plot_1_subplot1)
		
	
	for k in range(0,number_beta):	
		ARR_Pprop.append(float(Thrust[k])*velocity)
		Pshaft.append(float(RPM[k])*float(Q[k])*2*np.pi/60.)
		
		if epu_measurement_data == 0:
			PL_PMC.append(mc_loss_no_freewheel*mot_count*mc_resistance*(float(Current[k]/mot_count)/mc_loss_par)**2) 
			PL_PMOT.append(float(RPM[k])*mot_fe_brg_loss*mot_count + mot_count*mot_cu_res*float(Current[k]/mot_count)**2)
			PL_PMOT_CU.append(mot_count*mot_cu_res*float(Current[k]/mot_count)**2)
			PL_PMOT_FE_BEA.append(float(RPM[k])*mot_fe_brg_loss*mot_count)

			PL_Pbat.append((float((Pshaft[k] + PL_PMC[k] + PL_PMOT[k])/bat_volt_av/bat_packs_per_set/mot_count)**2)*bat_res*bat_packs_per_set*mot_count)
			Ptot.append(Pshaft[k]+PL_Pbat[k]+PL_PMC[k]+PL_PMOT[k])

			PL_PMOT_PMC_MEASURED.append(0.0)

		if epu_measurement_data == 1:
			PL_PMOT_PMC_MEASURED.append(INTERPOLATE_INPUT_POWER(np.array([Q[k], RPM[k]]))[0] - Pshaft[k])
			
			PL_PMC.append(0.0)
			PL_PMOT.append(0.0)
			PL_PMOT_CU.append(0.0)
			PL_PMOT_FE_BEA.append(0.0)

			PL_Pbat.append((float(INTERPOLATE_INPUT_POWER(np.array([Q[k], RPM[k]]))/bat_volt_av/bat_packs_per_set/mot_count)**2)*bat_res*bat_packs_per_set*mot_count)
			Ptot.append(Pshaft[k]+PL_Pbat[k]+PL_PMOT_PMC_MEASURED[k])
			#print("Q: ",Q[k],"RPM: ",RPM[k],"interpol. Input Power: ",INTERPOLATE_INPUT_POWER(np.array([Q[k], RPM[k]])), "P_Shaft: ",Pshaft[k])
			
		PL_Prop.append(Pshaft[k]-Pprop[i])
		Efficiency_EPU.append(Pprop[i]/Ptot[k])
		Efficiency_TOT.append(Climbrate[i]*mass*9.81/Ptot[k])

	plot_1_subplot6.plot(Dbeta[:], Ptot[:],'-',linewidth="3.0",color=lcolor)
	plot_1_subplot6.plot(Dbeta[:], Pshaft[:],'-',linewidth="1.0",label=diaglabel,color=lcolor)

	plt.legend(loc='upper right',fontsize=10)
	plt.xlabel('Dbeta [deg]',fontsize=10)
	plt.ylabel('Power [W]',fontsize=10)
	plt.grid(b=True,which='major',color='b',linestyle='--')
	plt.title('Power')
	
	sym_Ptot = matplotlib.lines.Line2D([], [], color='black',marker='',markersize=0,linestyle='solid',linewidth=3.0,label='P_tot')
	sym_Pshaft = matplotlib.lines.Line2D([], [], color='black',marker='',markersize=0,linestyle='solid',linewidth=1.0,label='P_shaft')

	handles = [sym_Ptot,sym_Pshaft]
	labels = [h.get_label() for h in handles]
	plot_1_subplot6.legend(handles=handles, labels=labels,fontsize=12)  

	for k in range(0,number_beta):
		Current_Battery.append(float(Ptot[k]/bat_volt_av))
	
	##### Current Motor #########################################
	try:
		dummy=plot_1_subplot5	
	except:
		plot_1_subplot5=plot_1.add_subplot(2, 3, 5,sharex=plot_1_subplot1)
			
	plot_1_subplot5.plot(Dbeta[:], Current[:],'-',linewidth=3.0,label=diaglabel,color=lcolor)	
	plot_1_subplot5.plot(Dbeta[:], Current_Battery[:],'-',linewidth=1.0,label=diaglabel,color=lcolor)	
	
	plt.xlabel('Dbeta [deg]',fontsize=10)
	plt.ylabel('Curent [A]',fontsize=10)
	plt.grid(b=True,which='major',color='b',linestyle='--')
	plt.title('Current (total)')
	
	sym_Current_nom = matplotlib.lines.Line2D([], [], color='red',marker='',markersize=0,linestyle='-.',linewidth=2.0,label='Current_Max')
	sym_Current_peak = matplotlib.lines.Line2D([], [], color='red',marker='',markersize=0,linestyle='-',linewidth=2.0,label='Current_Peak')
	sym_Mot = matplotlib.lines.Line2D([], [], color='black',marker='',markersize=0,linestyle='-',linewidth=3.0,label='Motor Current')
	sym_Bat = matplotlib.lines.Line2D([], [], color='black',marker='',markersize=0,linestyle='-',linewidth=1.0,label='Battery Current')	

	handles = [sym_Current_peak, sym_Current_nom,sym_Mot,sym_Bat]
	labels = [h.get_label() for h in handles]
	plot_1_subplot5.legend(handles=handles, labels=labels,fontsize=12) 
	
	
	if epu_measurement_data == 0:
		##### SECOND FIGURE ##########################################
		plot_2 = plt.figure(2, figsize=(15,12))
		plot_2.suptitle(ctitle, fontsize=12)

		##### Battery Loss #########################################
		try:
			dummy=plot_2_subplot1		
		except:
			plot_2_subplot1=plot_2.add_subplot(2, 2, 1)
		
		diaglabel = "P_prop: " + str(Pprop[i]) + 'W'
		plot_2_subplot1.plot(Dbeta[:], PL_Pbat[:],'-',label=diaglabel,color=lcolor)
		plt.xlabel('Dbeta [deg]',fontsize=10)
		plt.ylabel('Power Loss [W]',fontsize=10)
		plt.grid(b=True,which='major',color='b',linestyle='--')
		plt.title('Power Loss Battery')
		
		
		plot_2_subplot1.legend(loc='upper left',fontsize=12)  

		##### Motor Loss #########################################
		## Total loss #######
		try:
			dummy=plot_2_subplot2	
		except:
			plot_2_subplot2=plot_2.add_subplot(2, 2, 2,sharex=plot_2_subplot1)
			
		plot_2_subplot2.plot(Dbeta[:], PL_PMOT[:],'-',label=diaglabel,color=lcolor,linestyle='solid',linewidth=2.0)
		plt.xlabel('Dbeta [deg]',fontsize=10)
		plt.ylabel('Power Loss [W]',fontsize=10)
		plt.grid(b=True,which='major',color='b',linestyle='-')

		## Fe and Bearing loss #######
		plot_2_subplot2.plot(Dbeta[:], PL_PMOT_FE_BEA[:],'--',label=diaglabel,color=lcolor,linestyle='dashed',linewidth=1.0)	

		## Cu loss #######
		plot_2_subplot2.plot(Dbeta[:], PL_PMOT_CU[:],'.',label=diaglabel,color=lcolor,linestyle='dotted',linewidth=1.0,marker='',markersize=0)
		plt.title('Power Loss Motor')	

		sym_Pmot_tot = matplotlib.lines.Line2D([], [], color='black',marker='',markersize=0,linestyle='solid',linewidth=2.0,label='P-loss total')
		sym_Pmot_fe_bea = matplotlib.lines.Line2D([], [], color='black',marker='',markersize=0,linestyle='dashed',linewidth=1.0,label='P-loss bearing and FE-induction')
		sym_Pmot_cu = matplotlib.lines.Line2D([], [], color='black',marker='',markersize=0,linestyle='dotted',linewidth=1.0,label='P-loss copper')	

		handlesm = [sym_Pmot_tot,sym_Pmot_fe_bea,sym_Pmot_cu]
		labelsm = [h.get_label() for h in handlesm]
		plot_2_subplot2.legend(handles=handlesm, labels=labelsm,fontsize=12)  


		##### Motor controller Loss #########################################
		try:
			dummy=plot_2_subplot3	
		except:
			plot_2_subplot3=plot_2.add_subplot(2, 2, 3,sharex=plot_2_subplot1)
		plot_2_subplot3.plot(Dbeta[:], PL_PMC[:],'-',label=diaglabel,color=lcolor)
		plt.xlabel('Dbeta [deg]',fontsize=10)
		plt.ylabel('Power Loss [W]',fontsize=10)
		plt.grid(b=True,which='major',color='b',linestyle='--')
		plt.title('Power Loss Motor Controller')	

		##### Propeller Loss #########################################
		try:
			dummy=plot_2_subplot4		
		except:
			plot_2_subplot4=plot_2.add_subplot(2, 2, 4,sharex=plot_2_subplot1)
		
		
		plot_2_subplot4.plot(Dbeta[:], PL_Prop[:],'-',label=diaglabel,color=lcolor)
		plt.xlabel('Dbeta [deg]',fontsize=10)
		plt.ylabel('Power Loss [W]',fontsize=10)
		plt.grid(b=True,which='major',color='b',linestyle='--')
		plt.title('Power Loss Propeller')
	
	if epu_measurement_data == 1:
		##### FOURTH FIGURE ##########################################
		plot_4 = plt.figure(4, figsize=(15,12))
		plot_4.suptitle(ctitle, fontsize=12)

		##### Battery Loss #########################################
		try:
			dummy=plot_4_subplot1
		except:
			plot_4_subplot1=plot_4.add_subplot(2, 2, 1)
		
		plot_4_subplot1.plot(Dbeta[:], PL_Pbat[:],'-',label=diaglabel,color=lcolor)
		
		
		plt.xlabel('Dbeta [deg]',fontsize=10)
		plt.ylabel('Power Loss [W]',fontsize=10)
		plt.grid(b=True,which='major',color='b',linestyle='--')
		plt.title('Power Loss Battery')

		##### Motor Loss #########################################
		## Total loss #######
		try:
			dummy=plot_4_subplot2	
		except:
			plot_4_subplot2=plot_4.add_subplot(2, 2, 2,sharex=plot_4_subplot1)		

		plot_4_subplot2.plot(Dbeta[:], PL_PMOT_PMC_MEASURED[:],'-',label=diaglabel,color=lcolor)
		plt.xlabel('Dbeta [deg]',fontsize=10)
		plt.ylabel('Power Loss [W]',fontsize=10)
		plt.grid(b=True,which='major',color='b',linestyle='-')
		
	
	
		plt.title('Power Loss Motor and Controller Measured')	
	
		sym_Pmot_tot = matplotlib.lines.Line2D([], [], color='black',marker='',markersize=0,linestyle='solid',linewidth=1.0,label='P-loss total')

		handlesm = [sym_Pmot_tot]
		labelsm = [h.get_label() for h in handlesm]
		plot_4_subplot2.legend(handles=handlesm, labels=labelsm,fontsize=12)  
		
	
		##### Propeller Loss #########################################
		try:
			dummy=plot_4_subplot4	
		except:
			plot_4_subplot4=plot_4.add_subplot(2, 2, 4,sharex=plot_4_subplot1)
			
		plot_4_subplot4.plot(Dbeta[:], PL_Prop[:],'-',label=diaglabel,color=lcolor)
		plt.xlabel('Dbeta [deg]',fontsize=10)
		plt.ylabel('Power Loss [W]',fontsize=10)
		plt.grid(b=True,which='major',color='b',linestyle='--')
		plt.title('Power Loss Propeller')

	##### FIRST FIGURE ################################################
	
	# Determine Values with best EPU efficiency
	Maxeff_EPU.append(max(Efficiency_EPU[:]))
	Maxeff_EPU_Dbeta.append(Dbeta[Efficiency_EPU.index(max(Efficiency_EPU[:]))])
	Maxeff_EPU_RPM.append(RPM[Efficiency_EPU.index(max(Efficiency_EPU[:]))])
	Maxeff_EPU_Q.append(Q[Efficiency_EPU.index(max(Efficiency_EPU[:]))])
	Maxeff_EPU_Current.append(Current[Efficiency_EPU.index(max(Efficiency_EPU[:]))])
	Maxeff_EPU_PLbat.append(PL_Pbat[Efficiency_EPU.index(max(Efficiency_EPU[:]))])
	Maxeff_EPU_PLPMC.append(PL_PMC[Efficiency_EPU.index(max(Efficiency_EPU[:]))])
	Maxeff_EPU_PLPMOT.append(PL_PMOT[Efficiency_EPU.index(max(Efficiency_EPU[:]))])
	Maxeff_EPU_PL_Prop.append(PL_Prop[Efficiency_EPU.index(max(Efficiency_EPU[:]))])
	Maxeff_EPU_Ptot.append(Ptot[Efficiency_EPU.index(max(Efficiency_EPU[:]))])
	Maxeff_EPU_MOT_MC_MEASURED.append(PL_PMOT_PMC_MEASURED[Efficiency_EPU.index(max(Efficiency_EPU[:]))])
	
	Maxeff_prop_PLbat.append(PL_Pbat[Efficiency_prop.index(max(Efficiency_prop[:]))])
	Maxeff_prop_PLPMC.append(PL_PMC[Efficiency_prop.index(max(Efficiency_prop[:]))])
	Maxeff_prop_PLPMOT.append(PL_PMOT[Efficiency_prop.index(max(Efficiency_prop[:]))])
	Maxeff_prop_PL_Prop.append(PL_Prop[Efficiency_prop.index(max(Efficiency_prop[:]))])	
	Maxeff_prop_PLPMOT_PMC_MEASURED.append(PL_PMOT_PMC_MEASURED[Efficiency_prop.index(max(Efficiency_prop[:]))])
	
	# Determine Values with best Overall System Efficiency (including EPU and Aircraft)
	Maxeff_TOT.append(max(Efficiency_TOT[:]))
	Maxeff_TOT_Dbeta.append(Dbeta[Efficiency_TOT.index(max(Efficiency_TOT[:]))])
	Maxeff_TOT_RPM.append(RPM[Efficiency_TOT.index(max(Efficiency_TOT[:]))])
	Maxeff_TOT_Q.append(Q[Efficiency_TOT.index(max(Efficiency_TOT[:]))])
	Maxeff_TOT_Current.append(Current[Efficiency_TOT.index(max(Efficiency_TOT[:]))])
	Maxeff_TOT_PLbat.append(PL_Pbat[Efficiency_TOT.index(max(Efficiency_TOT[:]))])
	Maxeff_TOT_PLPMC.append(PL_PMC[Efficiency_TOT.index(max(Efficiency_TOT[:]))])
	Maxeff_TOT_PLPMOT.append(PL_PMOT[Efficiency_TOT.index(max(Efficiency_TOT[:]))])
	Maxeff_TOT_PL_Prop.append(PL_Prop[Efficiency_TOT.index(max(Efficiency_TOT[:]))])
	Maxeff_TOT_Ptot.append(Ptot[Efficiency_TOT.index(max(Efficiency_TOT[:]))])

	
	plot_1 = plt.figure(1)
	plot_1_subplot1.plot(Dbeta[:], Efficiency_EPU[:],'--',label="EPU Efficiency",color=lcolor)

	sym_Eprop = matplotlib.lines.Line2D([], [], color='black',marker='',markersize=0,linestyle='-',linewidth=1.0,label='Prop. Efficiency')
	sym_EEPU = matplotlib.lines.Line2D([], [], color='black',marker='',markersize=0,linestyle='--',linewidth=1.0,label='EPU Efficiency')
	sym_ETOT = matplotlib.lines.Line2D([], [], color='black',marker='',markersize=0,linestyle='-.',linewidth=1.0,label='TOT Efficiency')
	
	handles = [sym_Eprop,sym_EEPU]
	labels = [h.get_label() for h in handles]
	plot_1_subplot1.legend(handles=handles, labels=labels,fontsize=12)
	
	##### THIRD FIGURE ##########################################
	plot_3 = plt.figure(3, figsize=(15,12))
	ctitle = "Altitude above MSL="+str(act_alt)+" [m], Velocity= "+ str(velocity) + "[m/s], density= " + atmo[0] + " [kg/m^3], mue=" + atmo[1] + "[kg/m-s], c=" + atmo[2] + "[m/s]"
	diaglabel = "P_prop: " + str(Pprop[i]) + 'W, w= ' + "%.2f" % float(Climbrate[i]) + " m/s"
	plot_3.suptitle(ctitle, fontsize=12)


	try:
		dummy=plot_3_subplot1	
	except:
		plot_3_subplot1=plot_3.add_subplot(1, 1, 1)
	
	plot_3_subplot1.plot(Dbeta[:], Efficiency_TOT[:],'-.',color=lcolor)
	plt.ylim([0,1])
	plt.xlabel('Dbeta [deg]',fontsize=10)
	plt.ylabel('Efficiency [-]',fontsize=10)
	plt.grid(b=True,which='major',color='b',linestyle='--')
	plt.title('Efficiency')

	
	##### Efficiency PROP #########################################
	plot_3_subplot1.plot(Dbeta[:], Efficiency_prop[:],'-',label=diaglabel,color=lcolor)
	plt.ylim([0,1])
	plt.xlabel('Dbeta [deg]',fontsize=10)
	plt.ylabel('Efficiency [-]',fontsize=10)
	plt.grid(b=True,which='major',color='b',linestyle='--')
	plt.title('Efficiency')
	
	
	##### Efficiency EPU #########################################
	plot_3_subplot1.plot(Dbeta[:], Efficiency_EPU[:],'--',color=lcolor)
	plt.ylim([0,1])
	plt.xlabel('Dbeta [deg]',fontsize=10)
	plt.ylabel('Efficiency [-]',fontsize=10)
	plt.grid(b=True,which='major',color='b',linestyle='--')
	plt.title('Efficiency')

	handles = [sym_Eprop,sym_EEPU,sym_ETOT]
	labels = [h.get_label() for h in handles]
	plot_3_subplot1.legend(handles=handles, labels=labels,fontsize=12)	
	
	
	
	##### CSV ALL OUTPUT #####################################################	  
	
	for n in range(0,len(Dbeta)):
		csv_out.write(str(act_alt)+","+str(velocity)+","+"%.2f" % float(Climbrate[i])+","+str(Dbeta[n])+","+str(Efficiency_prop[n])+","+str(Efficiency_EPU[n])+","+str(Efficiency_TOT[n])+","+str(RPM[n])+","+str(ARR_Pprop[n])+","+str(Pshaft[n])+","+str(Ptot[n])+","+str(Thrust[n])+","+str(Q[n])+","+str(Current[n])+","+str(Current_Battery[n])+","+str(PL_Pbat[n])+","+str(PL_PMC[n])+","+str(PL_Prop[n])+","+str(PL_PMOT_CU[n])+","+str(PL_PMOT_FE_BEA[n])+","+str(PL_PMOT[n])+","+str(PL_PMOT_PMC_MEASURED[n])+","+str(CURR_CONST_MEASURED[n])+"\n")
	
	##### CSV MAXEFF OUTPUT #####################################################	 
	
	if climb_sink_flag == 1: ######### Calculating CLIMB and HOVER #############################
		##### Calculate the minimum necessary HOVER power
		if i == 0:
			
			i_min_Ptot = Ptot.index(min(Ptot[:])) ####### Index of minimum Total Power	
			csv_max_tot_eff_out.write(str(act_alt)+","+str(velocity)+","+"%.2f" % float(Climbrate[i])+","+str(Dbeta[i_min_Ptot])+","+str(Efficiency_prop[i_min_Ptot])+","+str(Efficiency_EPU[i_min_Ptot])+","+str("0.0")+","+str(RPM[i_min_Ptot])+","+str(ARR_Pprop[i_min_Ptot])+","+str(Pshaft[i_min_Ptot])+","+str(Ptot[i_min_Ptot])+","+str(Thrust[i_min_Ptot])+","+str(Q[i_min_Ptot])+","+str(Current[i_min_Ptot])+","+str(Current_Battery[i_min_Ptot])+","+str(PL_Pbat[i_min_Ptot])+","+str(PL_PMC[i_min_Ptot])+","+str(PL_Prop[i_min_Ptot])+","+str(PL_PMOT_CU[i_min_Ptot])+","+str(PL_PMOT_FE_BEA[i_min_Ptot])+","+str(PL_PMOT[i_min_Ptot])+","+str(PL_PMOT_PMC_MEASURED[i_min_Ptot])+","+str(CURR_CONST_MEASURED[i_min_Ptot])+ "\n")

		##### Calculate the minimum necessary CLIMB power
		if i > 0:
			i_max_TotEff= Efficiency_TOT.index(max(Efficiency_TOT[:])) ####### Index of maximum Total Efficiency
			csv_max_tot_eff_out.write(str(act_alt)+","+str(velocity)+","+"%.2f" % float(Climbrate[i])+","+str(Maxeff_TOT_Dbeta[i])+","+str(Efficiency_prop[i_max_TotEff])+","+str(Efficiency_EPU[i_max_TotEff])+","+str(max(Efficiency_TOT[:]))+","+str(Maxeff_TOT_RPM[i])+","+str(Pprop[i])+","+str(Pshaft[i_max_TotEff])+","+str(Ptot[i_max_TotEff])+","+str(Thrust[i_max_TotEff])+","+str(Q[i_max_TotEff])+","+str(Current[i_max_TotEff])+","+str(Current_Battery[i_max_TotEff])+","+str(PL_Pbat[i_max_TotEff])+","+str(PL_PMC[i_max_TotEff])+","+str(PL_Prop[i_max_TotEff])+","+str(PL_PMOT_CU[i_max_TotEff])+","+str(PL_PMOT_FE_BEA[i_max_TotEff])+","+str(PL_PMOT[i_max_TotEff])+","+str(PL_PMOT_PMC_MEASURED[i_max_TotEff])+ ","+str(CURR_CONST_MEASURED[i_max_TotEff])+ "\n")

	if climb_sink_flag == 0: ######### Calculating SINK and HOVER #####################################
		##### Calculate the minimum necessary HOVER power and reduced SINK power
		i_min_Ptot = Ptot.index(min(Ptot[:])) ####### Index of minimum Total Power	
		csv_max_tot_eff_out.write(str(act_alt)+","+str(velocity)+","+"%.2f" % float(Climbrate[i])+","+str(Dbeta[i_min_Ptot])+","+str(Efficiency_prop[i_min_Ptot])+","+str(Efficiency_EPU[i_min_Ptot])+","+str("0.0")+","+str(RPM[i_min_Ptot])+","+str(ARR_Pprop[i_min_Ptot])+","+str(Pshaft[i_min_Ptot])+","+str(Ptot[i_min_Ptot])+","+str(Thrust[i_min_Ptot])+","+str(Q[i_min_Ptot])+","+str(Current[i_min_Ptot])+","+str(Current_Battery[i_min_Ptot])+","+str(PL_Pbat[i_min_Ptot])+","+str(PL_PMC[i_min_Ptot])+","+str(PL_Prop[i_min_Ptot])+","+str(PL_PMOT_CU[i_min_Ptot])+","+str(PL_PMOT_FE_BEA[i_min_Ptot])+","+str(PL_PMOT[i_min_Ptot])+","+str(PL_PMOT_PMC_MEASURED[i_min_Ptot])+ ","+str(CURR_CONST_MEASURED[i_min_Ptot])+ "\n")
	

	#### Determine maximum value for diagram ordinate in plots ####
	if (np.max(ARR_Pprop) > Pmax):
		Pmax = np.max(ARR_Pprop)
		
	del CURR_CONST_MEASURED[:]
	del RPM[:]
	del Dbeta[:]
	del Q[:]
	del Efficiency_prop[:] 
	del Thrust[:] 
	del Current[:]
	del Current_Battery[:]
	
	del Pshaft [:]
	del ARR_Pprop [:]
	del Ptot [:]
	
	del PL_Pbat [:]
	del PL_PMC [:]
	del PL_PMOT [:]
	del PL_PMOT_CU [:]
	del PL_PMOT_FE_BEA [:]	
	del PL_Prop[:]
	del PL_PMOT_PMC_MEASURED[:]
	
	del Efficiency_EPU [:]
	del Efficiency_TOT [:]
	

#### Torque Limits in Plots for Torque, Current and Power ####
plot_1_subplot3.set_ylim([0,peakmoment*1.2])
plot_1_subplot5.set_ylim([0,peakcurrent_per_motor*2*1.2])
plot_1_subplot6.set_ylim([0,Pmax*2.5])


#### Motor Power Loss ####
#plot_4_subplot2.set_ylim([0,5000])
#### Battery Power Loss ####
#plot_4_subplot1.set_ylim([0,3000])
#### Propeller Power Loss ####
#plot_4_subplot4.set_ylim([0,12000])



csv_out.close()
	
plot_1 = plt.figure(1)

for i in range(0,len(Pprop)):
	Maxeff_prop_Pshaft.append(int(float(Pprop[i])/float(Maxeff_prop[i])))	
	Maxeff_EPU_Pshaft.append(int(float(Pprop[i])/float(Maxeff_EPU[i])))


### Print maxvalues first figure
#p1=plot_1.add_subplot(2, 3, 1)
#plt.plot(Maxeff_prop_Dbeta[:], Maxeff_prop[:],'-o',color='blue',label="Max. Prop Eff.")
#plt.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU[:],'-o',color='red',label="Max. EPU Eff.")
plot_1_subplot1.plot(Maxeff_prop_Dbeta[:], Maxeff_prop[:],'-o',color='blue',label="Max. Prop Eff.")
plot_1_subplot1.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU[:],'-o',color='red',label="Max. EPU Eff.")

#p2=plot_1.add_subplot(2, 3, 2,sharex=plot_1_subplot1)
#plt.plot(Maxeff_prop_Dbeta[:], Maxeff_prop_RPM[:],'-o',color='blue',label="Max. Prop. Eff.")
#plt.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU_RPM[:],'-o',color='red',label="Max. EPU Eff.")
#plt.legend(loc='upper right',fontsize=12)
plot_1_subplot2.plot(Maxeff_prop_Dbeta[:], Maxeff_prop_RPM[:],'-o',color='blue',label="Max. Prop. Eff.")
plot_1_subplot2.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU_RPM[:],'-o',color='red',label="Max. EPU Eff.")
plot_1_subplot2.legend(loc='upper right',fontsize=12)


#p3=plot_1.add_subplot(2, 3, 3,sharex=plot_1_subplot1)
#plt.plot(Maxeff_prop_Dbeta[:], Maxeff_prop_Q[:],'-o',color='blue',label="Max. Prop. Eff.")
#plt.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU_Q[:],'-o',color='red',label="Max. EPU Eff.")
plot_1_subplot3.plot(Maxeff_prop_Dbeta[:], Maxeff_prop_Q[:],'-o',color='blue',label="Max. Prop. Eff.")
plot_1_subplot3.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU_Q[:],'-o',color='red',label="Max. EPU Eff.")

plot_1_subplot3.plot([min_delta_beta,max_delta_beta], [maxmoment,maxmoment],linestyle="-.",color="r",linewidth=2.0,label="Max. Q")
plot_1_subplot3.plot([min_delta_beta,max_delta_beta], [peakmoment,peakmoment],linestyle="-",color="r",linewidth=2.0,label="Peak Q")

try:
	dummy=plot_1_subplot4	
except:
	plot_1_subplot4=plot_1.add_subplot(2, 3, 4,sharex=plot_1_subplot1)

plot_1_subplot4.plot(Maxeff_prop_Dbeta[:], Maxeff_prop_Thrust[:],'-o',label="Max. Eff.")
#plt.legend(loc='lower left',fontsize=10)

try:
	dummy=plot_1_subplot5
except:
	plot_1_subplot5=plot_1.add_subplot(2, 3, 5,sharex=plot_1_subplot1)


plot_1_subplot5.plot(Maxeff_prop_Dbeta[:], Maxeff_prop_Current[:],'-o',color='blue',label="Max. Prop. Eff.")
plot_1_subplot5.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU_Current[:],'-o',color='red',label="Max. EPU Eff.")
plot_1_subplot5.plot([min_delta_beta,max_delta_beta], [maxcurrent_per_motor*mot_count,maxcurrent_per_motor*mot_count],linestyle="-.",color="r",linewidth=2.0,label="Max. Current")
plot_1_subplot5.plot([min_delta_beta,max_delta_beta], [peakcurrent_per_motor*mot_count,peakcurrent_per_motor*mot_count],linestyle="-",color="r",linewidth=2.0,label="Peak Current")


try:
	dummy=plot_1_subplot6	
except:
	plot_1_subplot6=plot_1.add_subplot(2, 3, 6,sharex=plot_1_subplot1)


plot_1_subplot6.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU_Ptot[:],'-o',color='red',label="Max. EPU Eff.")
plt.savefig('plot1.png')

if epu_measurement_data == 0:
	 ### Print maxvalues second figure
	 plot_2 = plt.figure(2)

	 try:
		 dummy=plot_2_subplot1	
	 except:
		  plot_2_subplot1=plot_2.add_subplot(2, 2, 1)
	 plot_2_subplot1.plot(Maxeff_prop_Dbeta[:], Maxeff_prop_PLbat[:],'-o',color='blue',label="test")
	 plot_2_subplot1.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU_PLbat[:],'-o',color='red',label="test")

	 try:
		 dummy=plot_2_subplot2	
	 except:					     	      
		 plot_2_subplot2=plot_2.add_subplot(2, 2, 2,sharex=plot_2_subplot1)      
	 plot_2_subplot2.plot(Maxeff_prop_Dbeta[:], Maxeff_prop_PLPMOT[:],'-o',color='blue',label="Max. Prop. Eff.")
	 plot_2_subplot2.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU_PLPMOT[:],'-o',color='red',label="Max. EPU Eff.")

	 try:									   	    
		 dummy=plot_2_subplot3					   	    
	 except:					     	      		   	    
		 plot_2_subplot3=plot_2.add_subplot(2, 2, 3,sharex=plot_2_subplot1)	    
	 plot_2_subplot3.plot(Maxeff_prop_Dbeta[:], Maxeff_prop_PLPMC[:],'-o',color='blue',label="test")
	 plot_2_subplot3.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU_PLPMC[:],'-o',color='red',label="test")

	 try:									   	    
		 dummy=plot_2_subplot4					   	    	     
	 except:					     	      		   	    	     
		 plot_2_subplot4=plot_2.add_subplot(2, 2, 4,sharex=plot_2_subplot1)	    	     
	 plot_2_subplot4.plot(Maxeff_prop_Dbeta[:], Maxeff_prop_PL_Prop[:],'-o',color='blue',label="test")
	 plot_2_subplot4.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU_PL_Prop[:],'-o',color='red',label="test")
	 plt.savefig('plot2.png')
	 
if epu_measurement_data == 1:
	 ### Print maxvalues second figure
	 plot_4 = plt.figure(4)

	 try:									   	    	     	      
		 dummy=plot_4_subplot1					   	    	     	      
	 except:					     	      		   	    	     	      
		 plot_4_subplot1=plot_4.add_subplot(2, 2, 1)	    	     	      										     	      
	 plot_4_subplot1.plot(Maxeff_prop_Dbeta[:], Maxeff_prop_PLbat[:],'-o',color='blue',label="test")
	 plot_4_subplot1.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU_PLbat[:],'-o',color='red',label="test")

	 try:									   	    	     	      								               
		 dummy=plot_4_subplot2				   	    	     	      								               
	 except:					     	      		   	    	     	      								               
		 plot_4_subplot2=plot_4.add_subplot(2, 2, 2,sharex=plot_4_subplot1)	    	     	      										     	               
																					               
	 plot_4_subplot2.plot(Maxeff_prop_Dbeta[:], Maxeff_prop_PLPMOT_PMC_MEASURED[:],'-o',color='blue',label="Max. Prop. Eff.")
	 plot_4_subplot2.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU_MOT_MC_MEASURED[:],'-o',color='red',label="Max. EPU Eff.")
	 plot_4_subplot2.legend(loc='upper right',fontsize=12)

	 try:									   	    	     	      								                	
		 dummy=plot_4_subplot4				   	    	     	      								                	
	 except:					     	      		   	    	     	      								                	
		 plot_4_subplot4=plot_4.add_subplot(2, 2, 4,sharex=plot_4_subplot1)											        	
	 plot_4_subplot4.plot(Maxeff_prop_Dbeta[:], Maxeff_prop_PL_Prop[:],'-o',color='blue',label="test")
	 plot_4_subplot4.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU_PL_Prop[:],'-o',color='red',label="test")
	 plt.savefig('plot2.png')



#### Print values to the screen #########################
#print "Altitude [km]:", act_alt
#print "Velocity [m/s]:",velocity


print("\n###### RPM and DBeta combinations that lead to max. propeller efficiency for given velocity = " + str(velocity) + "[m/s]  and given Prop-Power [W] " + str(Pprop) + "############")
for i in range(0,len(Pprop)):	
	me=float(str(Maxeff_prop[i]))*100.0
	print("P-Prop:",Pprop[i],\
	"W, P-Shaft:",Maxeff_prop_Pshaft[i],\
	"W, Prop-Eff.:", "%.1f" % me,\
	"%, Speed:",str(float(Maxeff_prop_RPM[i])),\
	"RPM, Dbeta:",Maxeff_prop_Dbeta[i],\
	"deg, Prop-Loss:",Maxeff_prop_Pshaft[i]-Pprop[i],\
	"W, Current:",str(int(float(Maxeff_prop_Current[i]))),"A")
	

print("\n###### RPM and DBeta combinations that lead to max. EPU efficiency for given velocity = " + str(velocity) + "[m/s]  and given Prop-Power [W] " + str(Pprop) + "############")
for i in range(0,len(Pprop)):	
	mt=float(str(Maxeff_EPU[i]))*100.0
	print("P-Prop:",Pprop[i],\
	"W, P-Shaft:",Maxeff_EPU_Pshaft[i],\
	"W, EPU-Eff.:",  "%.1f" % mt,\
	"%, Speed:",str(float(Maxeff_EPU_RPM[i])),\
	"RPM, Dbeta:",Maxeff_EPU_Dbeta[i],\
	"deg, Prop-Loss:",Maxeff_EPU_Pshaft[i]-Pprop[i],\
	"W, Current:",str(int(float(Maxeff_EPU_Current[i]))),"A")


print("\n###### RPM and DBeta combinations that lead to max. overall system efficiency (including EPU and Aircraft) for given velocity = " + str(velocity) + "[m/s]  and given Prop-Power [W] " + str(Pprop) + "############")
for i in range(0,len(Pprop)):	
	print("P-Prop: ",Pprop[i],\
	"W, Tot-Eff System: ", "%.1f" % float(Maxeff_TOT[i]*100.0),\
	"%, Speed= ",str(float(Maxeff_TOT_RPM[i])),\
	"RPM, Dbeta= ",Maxeff_TOT_Dbeta[i],\
	"deg, Mot-Curr=",str(int(float(Maxeff_TOT_Current[i]))),"A")

### Print maxvalues third figure
plot_3 = plt.figure(3)

try:									  
	dummy=plot_3_subplot1					  
except: 								  
	plot_3_subplot1=plot_3.add_subplot(1, 1, 1)

plot_3_subplot1.plot(Maxeff_prop_Dbeta[:], Maxeff_prop[:],'-o',color='blue',label="Max. Prop Eff.")
plot_3_subplot1.plot(Maxeff_EPU_Dbeta[:], Maxeff_EPU[:],'-o',color='red',label="Max. EPU Eff.")
del Maxeff_TOT_Dbeta[0]
del Maxeff_TOT[0]
plot_3_subplot1.plot(Maxeff_TOT_Dbeta[:], Maxeff_TOT[:],'-o',color='green',label="Max. TOT Eff.")
plot_3_subplot1.legend(loc='lower right',fontsize=12)
plt.savefig('plot3.png')


if plotflag == 1:
	plt.show(block=False)
	_ = input("Press [enter] to close all figures.")

plt.close('all')

vel_dir = "vel_" + str("%.1f" % float(velocity)) + "_alt_" + str("%.1f" % float(act_alt))
cmd0_d="rm -rf vel_" + str("%.1f" % float(velocity)) + "_alt_" + str("%.1f" % float(act_alt))
cmd0="mkdir vel_" + str("%.1f" % float(velocity)) + "_alt_" + str("%.1f" % float(act_alt))



if(os.path.isdir(vel_dir)):
	os.system(cmd0_d)
	os.system(cmd0)

else:
	os.system(cmd0)


cmd1="mv plot3.png vel_" + str("%.1f" % float(velocity))+"_alt_" + str("%.1f" % float(act_alt))+".png"
os.system(cmd1)

cmd2="mv csv*csv out* vel*png plot*png vel_" + str("%.1f" % float(velocity)) + "_alt_" + str("%.1f" % float(act_alt))
os.system(cmd2)

if(os.path.isdir('All_Velocities')):
	os.system("rm -rf All_Velocities")
	os.system("mkdir All_Velocities")
else:
	os.system("mkdir All_Velocities")

cmd3="cp vel*/*alt_"+str("%.1f" % float(act_alt))+"*png All_Velocities"
os.system(cmd3)

if epu_measurement_data == 0:
	cmd4="cp input_calcprop.inp input_general.inp input_performance.inp motor propeller qcon.def /home/sven/Python/Propeller_Performance/formulas.png /home/sven/Python/Propeller_Performance/*py  vel_" + str("%.1f" % float(velocity)) + "_alt_" + str("%.1f" % float(act_alt))
if epu_measurement_data == 1:
	cmd4="cp " +str(fn_const)+" " +str(fn_power)+" input_calcprop.inp input_general.inp input_performance.inp motor propeller qcon.def /home/sven/Python/Propeller_Performance/formulas.png /home/sven/Python/Propeller_Performance/*py  vel_" + str("%.1f" % float(velocity)) + "_alt_" + str("%.1f" % float(act_alt))

os.system(cmd4)

csv_max_tot_eff_out.close()
