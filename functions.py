#!/usr/bin/python3
import datetime, time, os, os.path, string
import numpy as np
from subprocess import call
import matplotlib.pyplot as plt
from operator import add


#############################################################################################
def RUN_QPROP( velocity, Pprop,number_beta, min_delta_beta, max_delta_beta, deltabeta ):	
	os.system('rm QPROP.error')
	thrust = Pprop/velocity
	f_out = open('propcalc', 'w')
	for i in range(0,number_beta):
		#print min_delta_beta + i*deltabeta
		f_out.write("qprop propeller motor " + str(velocity) + " " + "0 0 " + str(float(min_delta_beta) + float(i*deltabeta)) + " " + str(thrust) +"\n")
	f_out.close()
	os.system('chmod u+x propcalc')
	command='./propcalc 1> out_'+str(Pprop)+'W'+str(" 2> QPROP.error")
	#print(command)
	os.system(command)
	return 0;

############################################################################################
def WRITE_QCON(act_alt):
	altitude=[0.,1500.,1900.,2500.,5000.,6000.,7500.,10000.,12500.,14000.,15000.,16000.,17500.,20000.,24000.]
	density=[1.225,1.05807,1.01665,0.956859,0.736116,0.659697,0.556624,0.412707,0.287262,0.226753,0.193674,0.16542,0.130576,0.0880349,0.04626]
	mue=[1.81206E-05,1.76298E-05,1.74976E-05,1.72983E-05,1.64531E-05,1.61084E-05,1.55837E-05,1.46884E-05,1.43226E-05,1.43226E-05,1.43226E-05,1.43226E-05,1.43226E-05,1.43226E-05,1.45483E-05]
	soundspeed=[340.,334.5,332.9,330.6,320.5,316.4,310.2,299.2,295.,295.,295.,295.,295.,295.,297.8]

	f_out_atmo = open('qcon.def','w')
	f_out_atmo.write('\n')
	f_out_atmo.write(str(np.interp(act_alt,altitude,density)) + '    ! rho  kg/m^3\n')
	f_out_atmo.write(str(np.interp(act_alt,altitude,mue)) + '  ! mu   kg/m-s\n')
	f_out_atmo.write(str(np.interp(act_alt,altitude,soundspeed)) + '    ! a    m/s\n')
	f_out_atmo.close()
	return(str(np.interp(act_alt,altitude,density)),str(np.interp(act_alt,altitude,mue)),str(np.interp(act_alt,altitude,soundspeed)));

################################################################################
def INTERPOL_DENSITY(act_alt):
	
	altitude=[0.,1500.,1900.,2500.,5000.,6000.,7500.,10000.,12500.,14000.,15000.,16000.,17500.,20000.,24000.]
	density=[1.225,1.05807,1.01665,0.956859,0.736116,0.659697,0.556624,0.412707,0.287262,0.226753,0.193674,0.16542,0.130576,0.0880349,0.04626]
	mue=[1.81206E-05,1.76298E-05,1.74976E-05,1.72983E-05,1.64531E-05,1.61084E-05,1.55837E-05,1.46884E-05,1.43226E-05,1.43226E-05,1.43226E-05,1.43226E-05,1.43226E-05,1.43226E-05,1.45483E-05]
	soundspeed=[340.,334.5,332.9,330.6,320.5,316.4,310.2,299.2,295.,295.,295.,295.,295.,295.,297.8]
	
	return(str(np.interp(act_alt,altitude,density)))

################################################################################
def CHECK_ERRORS():
	abort = 0
	exists = os.path.isfile('QPROP.error')
	if exists:
		errorcheck = open('QPROP.error', 'r')
		if "IEEE_INVALID_FLAG" in errorcheck.readline():
			print("\n### ERROR, CALCULATION ABORTED. DBETA IS PROBABLY BEYOND ACCEPTABLE RANGE! ###\n")
			os.system('rm -rf vel* All* out*W Max_tot_Eff.csv Reduced_Sink.csv')
			abort = 1
		errorcheck.close()

	exists = os.path.isfile('EPU_DATA.error')											   
	if exists:
		errorcheck = open('EPU_DATA.error', 'r')
		if "MOTOR EFFICIENCY MEASUREMENT DATA RANGE VIOLATED" in errorcheck.readline():
			print("\n### ERROR, CALCULATION ABORTED. EPU DATARANGE INSUFFICIENT! REDUCE CLIMBRATES OR MODIFY DBETA RANGE ###\n")
			os.system('rm -rf vel* All* out*W Max_tot_Eff.csv Reduced_Sink.csv')
			abort = 1
		errorcheck.close()
	
	return(abort)
